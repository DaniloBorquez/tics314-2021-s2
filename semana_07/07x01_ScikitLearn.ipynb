{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# Introducción a Machine Learning: Scikit Learn\n",
    "\n",
    "Ahora vamos a poner en práctica lo visto en clases para hacer un programa utilizando _Scikit Learn_, la librería más famosa para utilizar técnicas de _Machine Learning_ en Python. Primero que nada, recordemos que este programa va a **aprender** de datos ya existentes para hacer **predicciones** sobre datos que no conoce. Pensemos en un ejemplo simple: una regresión lineal. Imaginemos que tenemos un *dataset* muy sencillo:\n",
    "\n",
    "> Para ciertas propiedades, queremos predecir su valor según los metros cuadrados de la misma.\n",
    "\n",
    "Este dataset se puede graficar en un plano, en donde en el eje $X$ tenemos los metros cuadrados y en el eje $Y$ el precio. Cuando hacemos una regresión lineal, lo que deseamos es conocer la recta que pasa lo más cerca posible de un conjunto de puntos como se muestra en la imagen a continuación:\n",
    "\n",
    "<img src=\"img/linear_regression.png\" alt=\"Regresión Lineal\" style=\"width: 30%;\"/>\n",
    "\n",
    "Ahora, ¿para qué nos sirve esto? Efectivamente, ahora podremos predecir para cierta propiedad con *metros cuadrados* que no conozcamos, el precio que va a tener la misma. La idea intrínseca es que nuestro programa **aprendió** de datos conocidos y ahora puede hacer **predicciones** de datos que no conoce. Lo que recibe como *input* son los metros cuadrados de una propiedad y lo que vamos a predecir es el precio de aquella propiedad. En este caso queremos predecir un valor numérico en donde difícilmente nuestras predicciones nos van a dar el valor exacto, pero si un muy buen aproximado. Así, los modelos que predicen un valor numérico se llaman **Modelos de Regresión**.\n",
    "\n",
    "Sin embargo, una de las tareas más famosas delegadas a los algoritmos de *Machine Learning* son las tareas de **Clasificación**. En estas tareas, los algoritmos al recibir un *input* predicen una clase para ese *input*. El ejemplo más clásico es entrenar un clasificador de *spam* para el correo. Este sería un clasificador binario, ya que si le mostramos un correo el programa puede dar dos opciones: **Sí, es spam** o **No, no es spam**. En este caso, las clases serían **spam** y **no spam**.\n",
    "\n",
    "Ahora bien, un *input* puede caer en más de dos categorías. Un ejemplo clásico es clasificar dígitos escritos a mano. en este caso, al mostrarle un dígito el programa lo clasificará en alguna de las 10 clases, donde cada una corresponde a un dígito entre el 0 y el 9.\n",
    "\n",
    "Ojo que en todos estos ejemplos el programa probablemente aprenderá con ejemplos en los que ya conoce la respuesta:\n",
    "\n",
    "- Para la regresión lineal, los metros cuadrados vienen acompañados del precio.\n",
    "- Para el clasificador de *spam*, necesitaremos mostrarle al programa muchos correos, varios de ellos serán *spam* y varios no.\n",
    "- Para el detector de dígitos, necesitamos mostrar muchos ejemplos de números escritos a mano y decirle al programa que número es.\n",
    "\n",
    "Cuando el programa aprende de ejemplos donde ya conoce la respuesta, le llamamos aprendizaje **Supervisado**. Existe el aprendizaje No Supervisado, pero eso va mucho más allá del *scope* de estas clases. \n",
    "\n",
    "La idea de esta clase es usar la librería `scikit learn` de Python para entrenar y usar un modelo de *Machine Learning*. Así, al final de esta clase aprenderás a:\n",
    "\n",
    "- Explorar datos que utilizarás para entrenar y probar un programa de *Machine Learning*.\n",
    "- Entrenar un modelo utilizando `scikit learn`.\n",
    "- Medir lo acertado que es tu modelo.\n",
    "- Utilizar tu modelo para futuras predicciones."
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Los datos\n",
    "\n",
    "Para esta actividad vamos a usar el *Iris flower dataset*, que habla sobre flores del género Iris. El *dataset* contiene 50 muestras de cada una de tres especies de Iris (Setosa, Virginica y Versicolor). Se midió cuatro rasgos de cada muestra: el largo y ancho del sépalo y pétalo, en centímetros. Lo que haremos en esta actividad será **predecir el largo del sépalo de la especie Iris Setosa**. Para partir vamos a ver una imagen de la Iris Setosa:\n",
    "\n",
    "<img src=\"img/setosa.jpeg\" alt=\"Iris Setosa\" style=\"width: 20%;\"/>\n",
    "\n",
    "\n",
    "El *dataset* viene incluido en `scikit-learn`. Este contiene en la key `data` un arreglo de 4 columnas con el largo y ancho de sépalos y pétalos. Además se indica en bajo la key `target` el tipo de cada una de las flores:"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "from sklearn import datasets\n",
    "iris = datasets.load_iris()\n",
    "iris.keys()"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "El *dataset* contiene varias llaves, así que vamos a partir mostrando lo que contiene `data`:"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "iris['data']"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Notamos que este es un arreglo de ancho 4, que corresponden al ancho y largo de los sépalos y pétalos. Para saber cuál es cuál vamos a ver el nombre de las `features`:"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "iris.feature_names"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "**Observación**: las _features_ son las columnas de nuestro _dataset_. Una _feature_ se define como una propiedad medible o una característica de un fenómeno."
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Ahora comprendemos que la primera columna es el largo del sépalo, la segunda es el ancho del sépalo, la tercera es el largo del pétalo y la última el ancho del pétalo. También tenemos que filtrar el *dataset* para quedarnos solamente con las filas que corresponden a flores Iris Setosa:"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "import pandas as pd\n",
    "\n",
    "df = pd.DataFrame(iris['data'])\n",
    "df.columns = iris.feature_names\n",
    "df"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "iris.target_names"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "iris.target"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Aquí vemos que las primeras 50 columnas de nuestros datos corresponden a flores Iris Setosa (que es el elemento 0 de la lista en `target_names`. Con lo que ya aprendimos de numpy ahora vamos a filtrar los datos:"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "iris['target'] == 0"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# DataFrame de Iris Setosa\n",
    "iris_setosa = df[iris['target'] == 0]\n",
    "iris_setosa"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Ahora vamos a explorar visualmente los datos de la Iris Setosa. Recordemos que queremos predecir el largo del sépalo, así que vamos a graficar el largo del pétalo en función de cada otra columna."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "sns.scatterplot(data=iris_setosa, x=\"sepal width (cm)\", y=\"sepal length (cm)\")\n",
    "plt.xlabel(\"Ancho del Sépalo\")\n",
    "plt.ylabel(\"Largo del Sépalo\")\n",
    "plt.show()"
   ],
   "outputs": [],
   "metadata": {
    "scrolled": true
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "sns.scatterplot(data=iris_setosa, x=\"petal length (cm)\", y=\"sepal length (cm)\")\n",
    "plt.xlabel('Largo del Pétalo')\n",
    "plt.ylabel('Largo del Sépalo')"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "sns.scatterplot(data=iris_setosa, x=\"petal width (cm)\", y=\"sepal length (cm)\")\n",
    "plt.xlabel('Ancho del Pétalo')\n",
    "plt.ylabel('Largo del Sépalo')"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Visualmente podemos ver que el largo del sépalo parece estar relacionado al ancho del sépalo. Para explorar mejor esta idea vamos a buscar correlaciones. Haremos esto gracias a las funciones que nos provee `pandas`. Ahora usamos el método `corr()` sobre el dataframe para buscar las correlaciones."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "corr_matrix = iris_setosa.corr()\n",
    "corr_matrix\n",
    "#corr_matrix['sepal length (cm)'].sort_values(ascending=False)"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Aquí encontramos un resultado muy interesante. La correlación entre el ancho y el largo del sépalo es bastante alta. Por lo tanto, vamos a hacer una regresión lineal que en el eje $X$ tiene el ancho del pétalo y en el eje $Y$ tiene el largo."
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Entrenando y probando nuestro modelo\n",
    "\n",
    "Tenemos 50 registros que nos indican el ancho y largo del sépalo (la verdad no son tantos, pero para está actividad va a estar bien). Así que haremos algo muy común en el área de *Machine Learning*: vamos a separar nuestros datos en un set de entrenamiento y un set de prueba. \n",
    "\n",
    "La idea es escoger aleatoriamente un pequeño grupo de datos y lo vamos a separar. Este será nuestro set de prueba. Los otros datos, que sería el set de entrenamiento, lo vamos a utilizar para hacer la regresión lineal (es decir, encontrar la recta que que se ajusta mejor). Luego, para cada ancho de sépalo del set de prueba, vamos a \"predecir\" su largo con la regresión. Como conocemos la respuesta correcta, vamos a ir calculando el *mean squared error* para nuestro modelo. Para esto vamos a usar una función de la librería `scikit-learn`.\n",
    "\n",
    "La idea de separar en _dataset_ de entrenamiento y prueba es que podamos ver cómo funciona el modelo con datos que **no vio en la fase de entrenamiento**. Podría pasar que un modelo funcione súper bien sobre los datos de entrenamiento, pero funcione mal para datos que no ha visto. Como nosotros vamos a querer predecir sobre el modelo, es importante que generalice bien para datos que no ha visto."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "# La primera columna es el largo del sépalo y la segunda el ancho del sépalo\n",
    "# Recordemos que df tiene todos mis datos\n",
    "setosa_sepal_data = iris_setosa[['sepal width (cm)', 'sepal length (cm)']]\n",
    "\n",
    "# Queremos un set de prueba del 15% del tamaño\n",
    "train_set, test_set = train_test_split(setosa_sepal_data, test_size=0.15)"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Primero imprimiremos el set de entrenamiento:"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "train_set.describe()"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Y ahora el test de prueba, que notamos que es de tamaño 0.15 respecto al *dataset* original."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "test_set"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Ahora llegamos al momento de la verdad: **vamos a entrenar un modelo de regresión lineal usando `scikit-learn`**. Obviamente hay formas matemáticas de calcular una regresión lineal, o técnicas basadas en _Gradient Descent_; todo esto va más allá del _scope_ del curso, así que vamos a mantener esto más o menos como una caja negra. `scikit-learn` sabe manejar todo esto por debajo, y lo más importante es que entiendas la interfaz de esta librería.\n",
    "\n",
    "En general, vamos a importar algún modelo de los que ya tiene implementados, enseñarle los datos de prueba como un par $X$, $Y$, donde $X$ son los *input* (en este caso el ancho del sépalo) e $Y$ las respuestas para esos *inputs*. Para \"enseñarle\" al modelo usaremos la función `fit` y luego para predecir la función `predict`."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "from sklearn.linear_model import LinearRegression\n",
    "\n",
    "# Vamos a separar la respuesta de nuestras features\n",
    "X_train = train_set[['sepal width (cm)']]\n",
    "y_train = train_set[['sepal length (cm)']]\n",
    "\n",
    "lin_reg = LinearRegression()\n",
    "\n",
    "lin_reg.fit(X_train, y_train)"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Muy bien, hemos entrenado nuestro modelo! Ahora vamos a hacer una predicción sobre él. Preguntémosle la medida del largo del sépalo para un ancho de 4, 5 y 6 cm:"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "lin_reg.predict([[4], [5], [6]])"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Como vemos, las respuestas son súper razonables. Aquí hay que hacer una acotación: el parámetro de la función predict es una lista de listas (o arreglo de arreglos), esto es porque podemos pasar hartos *inputs* a la vez, y cada uno de estos puede tener más de una dimensión. En este caso estamos entregando 3 *inputs* de dimensión 1. Ahora agreguemos la recta al gráfico para ver si hace sentido: "
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "sns.scatterplot(data=iris_setosa, x=\"sepal width (cm)\", y=\"sepal length (cm)\")\n",
    "plt.xlabel(\"Ancho del Sépalo\")\n",
    "plt.ylabel(\"Largo del Sépalo\")\n",
    "\n",
    "setosa_sepal_width = iris_setosa[[\"sepal width (cm)\"]]\n",
    "setosa_sepal_length = iris_setosa[[\"sepal length (cm)\"]]\n",
    "\n",
    "plt.plot(setosa_sepal_width, lin_reg.predict(setosa_sepal_width), color=\"orange\")"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Antes de seguir, vamos a explorar los coeficientes de la regresión lineal:"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# La pendiente\n",
    "lin_reg.coef_"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# El coeficiente de posición\n",
    "lin_reg.intercept_"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Ahora usaremos nuestros datos de prueba para entender que tan correcta es nuestra predicción. Para esto vamos a predecir los largos para cada ancho de prueba, y dado que conocemos las respuestas, calcularemos el error medio cuadrado con la función `mean_squared_error` que ya está incluida en `scikit-learn`:"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "from sklearn.metrics import mean_squared_error\n",
    "\n",
    "# Recordemos que test_set tiene nuestras observaciones de prueba\n",
    "# Predecimos sobre todos los input y mostramos los resultados\n",
    "X_test = test_set[['sepal width (cm)']]\n",
    "y_pred = lin_reg.predict(X_test)\n",
    "y_pred"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "y_test = test_set[['sepal length (cm)']]\n",
    "y_test"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# Ahora calculamos el error medio cuadrado\n",
    "# El primer parámetro de la función son las respuestas reales\n",
    "lin_mse = mean_squared_error(y_test, y_pred)\n",
    "lin_mse"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Para darle más significado a esta respuesta vamos a sacar la raíz cuadrada de este valor:"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "import numpy as np\n",
    "\n",
    "np.sqrt(lin_mse)"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Esto nos dice que en promedio estamos desviados 0.24 centímetros en nuestras predicciones. Nada mal considerando el rango en el que se mueven los largos de los sépalos."
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Una mejor forma de evaluar: *Cross-Validation*\n",
    "\n",
    "Una forma muy recurrente de evaluar algoritmos de *Machine Learning* es usar *Cross-Validation*. La idea es dividir los datos en $X$ partes. Luego, cada una de estas partes va a actuar como set de prueba, mientras las $X-1$ partes restantes actuarán como set de entrenamiento. Así, vamos a tener $X$ puntajes distintos. Aquí podemos tener una idea mucha más clara de la *performance* de nuestro modelo."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "from sklearn.model_selection import cross_val_score\n",
    "\n",
    "#Instanciamos un modelo\n",
    "lin_reg = LinearRegression()\n",
    "\n",
    "# Recordemos que en la variable iris_setosa tenemos nuestras 50 observaciones\n",
    "# No sirve como parámetro mean_squared_error\n",
    "\n",
    "X = iris_setosa[['sepal width (cm)']]\n",
    "y = iris_setosa[['sepal length (cm)']]\n",
    "\n",
    "scores = cross_val_score(lin_reg, X, y, scoring='neg_mean_squared_error', cv=8)\n",
    "\n",
    "lin_rmse_scores = np.sqrt(-scores)\n",
    "lin_rmse_scores"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "scores"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Finalmente, veamos el promedio y la desviación estándar de estos resultados:"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "print(lin_rmse_scores.mean())\n",
    "print(lin_rmse_scores.std())"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Algunas ideas de cómo seguir\n",
    "\n",
    "En este *Notebook* dimos una introducción simple a *Machine Learning* pero que a la vez te permite seguir aprendiendo por tu cuenta. La interfaz de SciKit Learn que utiliza `fit`- `predict` es ampliamente conocida y seguramente vas a encontrar mucha documentación en internet que te va a permitir seguir aprendiendo sobre el tema.\n",
    "\n",
    "Una parte muy importante de esto es entender cuando usar qué modelo, puesto que hay algunos que funcionan muy bien para detección de imagenes, otros para trabajar sobre texto, otros para hacer análisis de datos, etc.\n",
    "\n",
    "A su vez, también tienes que tener en cuenta que no todo problema necesita herramientas de Machine Learning. Quizás una solución menos compleja funcione igual de bien que un algoritmo más sofisticado que te va a tomar más tiempo de desarrollar (por ejemplo, la recolección de datos y la forma de serializarlos siempre es un problema muy complejo!). Además la solución estándar puede servir como baseline para que compares respecto a la solución sofisticada. Por eso, \"_do the unintelligent thing first_\".\n",
    "\n",
    "Finalmente, te dejamos una bibliografía que te será de mucha utilidad para seguir por tu cuenta.\n",
    "\n",
    "## Bibliografía\n",
    "\n",
    "- Hands-On Machine Learning with Scikit-Learn, Keras & TensorFlow (Aurélien Géron). Este es un muy buen libro que explica cómo hacer todas estas cosas utilizando herramientas relacionadas con Python. Incluso tiene una muy buena sección de Redes Neuronales.\n",
    "- An Introduction to Statistical Learning (James, Witten, Hastie, Tibshirani). Este libro habla sobre los modelos estándar de *Machine Learning* desde un punto de vista más teórico, pero bien amigable.\n",
    "- Foundations of Machine Learning (Afshin Rostamizadeh, Ameet Talwalkar & Mehryar Mohri). Este libro te cuenta toda la teoría detrás de los modelos, pero es bien espeso. Si realmente te gusta la teoría y las demostraciones espesas, puedes consultarlo.\n",
    "- [Towards Data Science](https://towardsdatascience.com/). Este blog de Medium tiene muy buen material sobre el tema. Cuando encuentres un tutorial acá, date el tiempo de leerlo y programarlo tú. Puedes intrusear y ver qué cosas de interés encuentras.\n",
    "- [Los videos de redes neuronales de 3Blue1Brown](https://www.youtube.com/playlist?list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi). Esta serie de videos explica de una forma muy amigable lo que es una red neuronal. Sirven para entender que realmente esto no es complejo y que con lo que sabemos podemos empezar a crear nuestras propias herramientas.\n",
    "- [El canal de StatQuest](https://www.youtube.com/user/joshstarmer). Este canal expica varios conceptos relacionados al área de _Machine Learning_ de un punto de vista más estadístico.\n",
    "- Una vez que conoces los conceptos y vayas manos a la obra, no te olvides de tus buenos amigos Google y StackOverflow."
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Agradecimientos especiales\n",
    "\n",
    "Además de planificarlo con el profesor Juan Reutter, muchas de las ideas de este notebook vinieron de conversaciones con Nebil Kawas y David Gómez."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [],
   "outputs": [],
   "metadata": {}
  }
 ],
 "metadata": {
  "kernelspec": {
   "name": "python3",
   "display_name": "Python 3.8.0 64-bit"
  },
  "language_info": {
   "name": "python",
   "version": "3.8.0",
   "mimetype": "text/x-python",
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "pygments_lexer": "ipython3",
   "nbconvert_exporter": "python",
   "file_extension": ".py"
  },
  "interpreter": {
   "hash": "aee8b7b246df8f9039afb4144a1f6fd8d2ca17a180786b69acc140d282b71a49"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}